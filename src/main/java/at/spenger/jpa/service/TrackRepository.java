package at.spenger.jpa.service;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import at.spenger.jpa.model.Artist;
import at.spenger.jpa.model.Track;

public interface TrackRepository extends CrudRepository<Track, Integer>{
	@Query("select a from Track a where a.name like :name")
	List<Track> findByNameLike(@Param("name") String name);
}
