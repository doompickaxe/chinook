package at.spenger.jpa.service;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import at.spenger.jpa.model.Playlist;

public interface PlayListRepository extends CrudRepository<Playlist, Integer> {
	List<Playlist> findAll();
}
