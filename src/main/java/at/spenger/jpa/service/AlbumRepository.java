package at.spenger.jpa.service;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import at.spenger.jpa.model.Album;
import at.spenger.jpa.model.Artist;

public interface AlbumRepository extends CrudRepository<Album, Integer> {
	@Query("select a from Album a where a.title like :name")
	List<Album> findByNameLike(@Param("name") String name);
}
