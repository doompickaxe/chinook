package at.spenger.jpa.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import at.spenger.jpa.model.Artist;
import at.spenger.jpa.model.Track;


@Component("trackService")
@Transactional	// Spring provides a transaction manager specifically for JPA.
public class TrackServiceImpl implements TrackService {
	private final TrackRepository trackRepository;
	@Autowired
	public TrackServiceImpl(TrackRepository trackRepository) {
		this.trackRepository = trackRepository;
	}

	@Override
	@Transactional(readOnly=true)
	public List<Track> findByNameLike(String name) {
		return trackRepository.findByNameLike(name);
	}
	@Override
	@Transactional(readOnly=true)
	public Track findOne(int id) {
		return trackRepository.findOne(id);
	}
	@Override
	public Track save(Track a) {
		return trackRepository.save(a);
	}
	@Override
	public void delete(int id) {
		trackRepository.delete(id);
	}
	@Override
	@Transactional(readOnly=true)
	public long length() {
		return trackRepository.count();
	}
	

}