package at.spenger.jpa.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import at.spenger.jpa.model.Album;


@Component("albumService")
@Transactional	// Spring provides a transaction manager specifically for JPA.
public class AlbumServiceImpl implements AlbumService {
	private final AlbumRepository albumRepository;
	@Autowired
	public AlbumServiceImpl(AlbumRepository albumRepository) {
		this.albumRepository = albumRepository;
	}

	@Override
	@Transactional(readOnly=true)
	public List<Album> findByNameLike(String name) {
		return albumRepository.findByNameLike(name);
	}
	@Override
	@Transactional(readOnly=true)
	public Album findOne(int id) {
		return albumRepository.findOne(id);
	}
	@Override
	public Album save(Album a) {
		return albumRepository.save(a);
	}
	@Override
	public void delete(int id) {
		albumRepository.delete(id);
	}
	@Override
	@Transactional(readOnly=true)
	public long length() {
		return albumRepository.count();
	}
	

}
