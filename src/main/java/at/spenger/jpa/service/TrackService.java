package at.spenger.jpa.service;

import java.util.List;

import at.spenger.jpa.model.Track;

import java.util.List;

import at.spenger.jpa.model.Artist;

public interface TrackService  {
	List<Track> findByNameLike(String name);
	Track findOne(int id);
	Track save(Track a);
	void delete(int id);
	long length();
}
