package at.spenger.jpa;

import java.beans.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import at.spenger.jpa.model.Album;
import at.spenger.jpa.model.Artist;
import at.spenger.jpa.model.Playlist;
import at.spenger.jpa.model.Track;
import at.spenger.jpa.service.AlbumRepository;
import at.spenger.jpa.service.AlbumService;
import at.spenger.jpa.service.ArtistRepository;
import at.spenger.jpa.service.ArtistService;
import at.spenger.jpa.service.PlayListRepository;
import at.spenger.jpa.service.PlayListService;
import at.spenger.jpa.service.TrackRepository;
import at.spenger.jpa.service.TrackService;


@Import(Config.class)
public class Application implements CommandLineRunner {

	@Autowired
	private PlayListService playListService;
	@Autowired
	private ArtistRepository artistRepository;
	@Autowired
	private ArtistService artistService;
	@Autowired
	private AlbumService albumService;
	@Autowired
	private AlbumRepository albumRepository;
	@Autowired
	private TrackRepository trackRepository;
	
	
	
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

	@Override
	@Transactional
	public void run(String... arg0) throws Exception {
		CreateArtist();
		ReadArtist();
	}
	
	private void CreateArtist() {
		
		
		
		List<Album> al = albumService.findByNameLike("In The Court Of The Crimson King");
		albumRepository.delete(al.get(0));
		
		List<Artist> l = artistService.findByNameLike("King Crimson");
		artistRepository.delete(l.get(0));
		
		
		
		
		Artist king=new Artist();
		Album court=new Album();
		ArrayList<Track> tracks=new ArrayList<Track>();
		tracks.add(new Track("21st Century Schizoid Man"));
		tracks.add(new Track("I Talk To The Wind"));
		tracks.add(new Track("March For No Reason"));
		tracks.add(new Track("Moonchild"));
		tracks.add(new Track("The Court Of The Crimson King"));
		
		for(Track t : tracks)
			trackRepository.save(t);
		
		court.setTracks(tracks);
		court.setTitle("In The Court Of The Crimson King");
		king.setName("King Crimson");
		artistRepository.save(king);
		court.setArtist(king);
		albumRepository.save(court);
		
	}
	
	private void ReadArtist(){
		List<Artist> l = artistService.findByNameLike("King Crimson");
		System.out.println(l.size()+" "+l.get(0).getName());
	}
}
