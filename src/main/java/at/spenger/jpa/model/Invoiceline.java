package at.spenger.jpa.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the invoiceline database table.
 * 
 */
@Entity
@NamedQuery(name="Invoiceline.findAll", query="SELECT i FROM Invoiceline i")
public class Invoiceline implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int invoiceLineId;

	private int quantity;

	private BigDecimal unitPrice;

	//bi-directional many-to-one association to Track
	@ManyToOne
	@JoinColumn(name="TrackId")
	private Track track;

	//bi-directional many-to-one association to Invoice
	@ManyToOne
	@JoinColumn(name="InvoiceId")
	private Invoice invoice;

	public Invoiceline() {
	}

	public int getInvoiceLineId() {
		return this.invoiceLineId;
	}

	public void setInvoiceLineId(int invoiceLineId) {
		this.invoiceLineId = invoiceLineId;
	}

	public int getQuantity() {
		return this.quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getUnitPrice() {
		return this.unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	public Track getTrack() {
		return this.track;
	}

	public void setTrack(Track track) {
		this.track = track;
	}

	public Invoice getInvoice() {
		return this.invoice;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}

}