package at.spenger.jpa.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the track database table.
 * 
 */
@Entity
@NamedQuery(name="Track.findAll", query="SELECT t FROM Track t")
public class Track implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int trackId;

	private int bytes;

	private String composer;

	private int milliseconds;

	private String name;

	private BigDecimal unitPrice;

	//bi-directional many-to-one association to Invoiceline
	// mapped entity
	@OneToMany(mappedBy="track")
	private List<Invoiceline> invoicelines;

	//bi-directional many-to-many association to Playlist
	// mapped entity
	@ManyToMany(mappedBy="tracks")
	private List<Playlist> playlists;

	//bi-directional many-to-one association to Mediatype
	@ManyToOne
	@JoinColumn(name="MediaTypeId")
	private Mediatype mediatype;

	//bi-directional many-to-one association to Album
	@ManyToOne
	@JoinColumn(name="AlbumId")
	private Album album;

	//bi-directional many-to-one association to Genre
	@ManyToOne
	@JoinColumn(name="GenreId")
	private Genre genre;

	public Track() {
	}
	
	public Track(String name) {
		setName(name);
		Mediatype media=new Mediatype();
		media.setMediaTypeId(1);
		setMediatype(media);
		setUnitPrice(new BigDecimal(1));
	}

	public int getTrackId() {
		return this.trackId;
	}

	public void setTrackId(int trackId) {
		this.trackId = trackId;
	}

	public int getBytes() {
		return this.bytes;
	}

	public void setBytes(int bytes) {
		this.bytes = bytes;
	}

	public String getComposer() {
		return this.composer;
	}

	public void setComposer(String composer) {
		this.composer = composer;
	}

	public int getMilliseconds() {
		return this.milliseconds;
	}

	public void setMilliseconds(int milliseconds) {
		this.milliseconds = milliseconds;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getUnitPrice() {
		return this.unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	public List<Invoiceline> getInvoicelines() {
		return this.invoicelines;
	}

	public void setInvoicelines(List<Invoiceline> invoicelines) {
		this.invoicelines = invoicelines;
	}

	public Invoiceline addInvoiceline(Invoiceline invoiceline) {
		getInvoicelines().add(invoiceline);
		invoiceline.setTrack(this);

		return invoiceline;
	}

	public Invoiceline removeInvoiceline(Invoiceline invoiceline) {
		getInvoicelines().remove(invoiceline);
		invoiceline.setTrack(null);

		return invoiceline;
	}

	public List<Playlist> getPlaylists() {
		return this.playlists;
	}

	public void setPlaylists(List<Playlist> playlists) {
		this.playlists = playlists;
	}

	public Mediatype getMediatype() {
		return this.mediatype;
	}

	public void setMediatype(Mediatype mediatype) {
		this.mediatype = mediatype;
	}

	public Album getAlbum() {
		return this.album;
	}

	public void setAlbum(Album album) {
		this.album = album;
	}

	public Genre getGenre() {
		return this.genre;
	}

	public void setGenre(Genre genre) {
		this.genre = genre;
	}

}