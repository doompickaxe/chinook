package at.spenger.jpa;


import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import at.spenger.jpa.Application;
import at.spenger.jpa.model.Album;
import at.spenger.jpa.model.Artist;
import at.spenger.jpa.model.Track;
import at.spenger.jpa.service.AlbumRepository;
import at.spenger.jpa.service.AlbumService;
import at.spenger.jpa.service.ArtistRepository;
import at.spenger.jpa.service.ArtistService;
import at.spenger.jpa.service.PlayListService;
import at.spenger.jpa.service.TrackService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class ApplicationTests {
	@Autowired
	private PlayListService playListService;
	@Autowired
	private ArtistRepository artistRepository;
	@Autowired
	private ArtistService artistService;
	@Autowired
	private AlbumService albumService;
	@Autowired
	private AlbumRepository albumRepository;
	@Autowired
	private TrackService trackService;
	
	@Test
	public void ArtistAdded() {
		List<Artist> l = artistService.findByNameLike("King Crimson");
		assertThat(l.get(0).getName(), is(equalTo("King Crimson")));
	}
	
	@Test
	public void AlbumAdded() {
		List<Album> l = albumService.findByNameLike("In The Court Of The Crimson King");
		assertThat(l.get(0).getTitle(), is(equalTo("In The Court Of The Crimson King")));
	}
	
	@Test
	public void ArtistDeleted() {
		List<Artist> l = artistService.findByNameLike("King Crimson");
		assertThat(l.size(), is(equalTo(1)));
	}
	
	@Test
	public void AlbumDeleted() {
		List<Album> l = albumService.findByNameLike("In The Court Of The Crimson King");
		assertThat(l.size(), is(equalTo(1)));
	}

}
